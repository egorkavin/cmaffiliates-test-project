import localFont from 'next/font/local';

import './globals.css';

export const metadata = {
  title: 'CMaffiliates Test Task',
  description: 'Test task by Yehor Azyrankulov',
};

const helvetica = localFont({
  src: [
    {
      path: '../../public/fonts/HelveticaNowDisplay-Regular.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: '../../public/fonts/HelveticaNowDisplay-Medium.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: '../../public/fonts/HelveticaNowDisplay-Bold.woff2',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font',
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className={`${helvetica.variable}`}>
      <body>{children}</body>
    </html>
  );
}
